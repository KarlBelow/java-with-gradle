package kbelow.java.with.gradle;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ExampleJavaClassTest {

   @Test
   void testJustReturnTrue() {
      var testObject = new ExampleJavaClass();
      assertTrue(testObject.justReturnTrue());
   }
}

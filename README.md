# A simple Java project with Gradle build script

This is a simple Java project together with an example Gradle build script and a .gitlab-ci.yml file.

This project makes usage of:
* JDK 10
* JUnit 5
* Gradle 4.10
